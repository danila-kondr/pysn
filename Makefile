.PHONY: all clean
.SUFFIXES: .lem .re

all: makeheaders lemon parser.c scanner.c parser.h test pysn2latex

CSN_OBJS=token.o node.o buffer.o parser.o scanner.o pysn.o

TEST_OBJS=$(CSN_OBJS) test.o
test: $(TEST_OBJS)
	$(CC) -g -o test $(TEST_OBJS)

CSN2LATEX_OBJS=$(CSN_OBJS) pysn2latex.o
pysn2latex: $(CSN2LATEX_OBJS)
	$(CC) -g -o pysn2latex $(CSN2LATEX_OBJS)

.lem.c:
	./lemon -m $<

parser.h: parser.c
	./makeheaders $<

.re.c:
	re2c -o $@ $<

.c.o:
	$(CC) -g -c -o $@ $<


lemon: lemon.c
	$(CC) -o lemon lemon.c	

makeheaders: makeheaders.c
	$(CC) -o makeheaders makeheaders.c

clean:
	rm lemon makeheaders test pysn2latex *.o
