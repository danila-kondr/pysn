#ifndef _SCANNER_H_
#define _SCANNER_H_

#include "token.h"

struct Scanner {
	const char *cursor, *marker, *limit;
};

void PySN_InitScanner(struct Scanner *scanner, const char *input, size_t len);
int PySN_NextToken(struct Scanner *scanner, struct Token **pToken);

#endif
