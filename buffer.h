#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <stddef.h>
#include <stdint.h>

struct Buffer {
	size_t size;
	size_t cap;
	char *str;
};

void PySN_InitBuffer(struct Buffer *pBuf);
void PySN_BufferPut(struct Buffer *pBuf, void *mem, size_t size);
void PySN_BufferPutString(struct Buffer *pBuf, const char *str);
void PySN_ClearBuffer(struct Buffer *pBuf);

#endif
