#include "token.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Token *PySN_CreateToken(int type, const char *str)
{
	return PySN_CreateTokenN(type, str, strlen(str));
}

struct Token *PySN_CreateTokenN(int type, const char *str, size_t len)
{
	struct Token *result = NULL;
	size_t i;

	if (len >= MAX_TOKEN_LENGTH) {
		fprintf(stderr, 
			"pysn: error during create token: "
			"too long token string: %d > %d, "
			"will be truncated to %d\n",
			len, MAX_TOKEN_LENGTH - 1, MAX_TOKEN_LENGTH - 1
		);
		len = MAX_TOKEN_LENGTH - 1;
	}

	if (!(result = malloc(sizeof(struct Token)))) {
		perror("pysn: error during allocating token");
		exit(1);
	}

	result->type = type;
	for (i = 0; i < len; i++)
		result->str[i] = str[i];
	result->str[i] = '\0';

	return result;
}

void PySN_InitToken(struct Token *token, int type, const char *str)
{
	PySN_InitTokenN(token, type, str, strlen(str));
}

void PySN_InitTokenN(struct Token *token, int type,
	const char *str, size_t len)
{
	size_t i;

	if (len >= MAX_TOKEN_LENGTH) {
		fprintf(stderr, "pysn: error during initializing of token: "
				"%d > %d, will be truncated to %d\n",
				len, MAX_TOKEN_LENGTH - 1,
				MAX_TOKEN_LENGTH - 1);
		len = MAX_TOKEN_LENGTH - 1;
	}

	token->type = type;
	for (i = 0; i < len; i++)
		token->str[i] = str[i];
	token->str[i] = '\0';
}
