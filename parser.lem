%name PySN_Parse
%token_prefix TOKEN_

%token_type { struct Token * }
%type expression { struct Node * }
%type formula { struct Node * }

%token_destructor { free($$); }
%destructor expression { }
%destructor formula { }

%extra_context { struct ParseContext *ctx }

%include {
#include "token.h"
#include "node.h"
#include "scanner.h"

#if INTERFACE
#include <stdio.h>
#endif
#include <stdlib.h>
#include <string.h>

#if INTERFACE
struct ParseContext {
	struct Node *result;
};
#endif

struct Node *PySN_CreateAST(const char *csn);
}

%left EQ NE LE GE LT GT IN.
%left PLUS MINUS.
%left MULTIPLY DIVIDE CDOT.
%right ASSIGN.
%right COMMA.

%token LEFT_PAR RIGHT_PAR SEMICOLON VAR NUM.

formula ::= expression(E).
{
	/* Here we must convert AST to one of the formats
         * (LaTeX, MathML or something else)
         * according the rules. */
	ctx->result = E;
}

expression(E) ::= expression(A) EQ expression(B).
{
	E = PySN_CreateNode(NODE_EQ, 2, A, B);
}

expression(E) ::= expression(A) NE expression(B).
{
	E = PySN_CreateNode(NODE_NE, 2, A, B);
}

expression(E) ::= expression(A) LE expression(B).
{
	E = PySN_CreateNode(NODE_LE, 2, A, B);
}

expression(E) ::= expression(A) GE expression(B).
{
	E = PySN_CreateNode(NODE_GE, 2, A, B);
}

expression(E) ::= expression(A) LT expression(B).
{
	E = PySN_CreateNode(NODE_LT, 2, A, B);
}

expression(E) ::= expression(A) GT expression(B).
{
	E = PySN_CreateNode(NODE_GT, 2, A, B);
}

expression(E) ::= expression(A) IN expression(B).
{
	E = PySN_CreateNode(NODE_IN, 2, A, B);
}

expression(E) ::= expression(A) PLUS expression(B).
{
	E = PySN_CreateNode(NODE_ADD, 2, A, B);
}

expression(E) ::= expression(A) MINUS expression(B).
{
	E = PySN_CreateNode(NODE_SUB, 2, A, B);
}

expression(E) ::= expression(A) MULTIPLY expression(B).
{
	E = PySN_CreateNode(NODE_MUL, 2, A, B);
}

expression(E) ::= expression(A) DIVIDE expression(B).
{
	E = PySN_CreateNode(NODE_DIV, 2, A, B);
}

expression(E) ::= expression(A) CDOT expression(B).
{
	E = PySN_CreateNode(NODE_CDOT, 2, A, B);
}

expression(E) ::= expression(A) ASSIGN expression(B).
{
	E = PySN_CreateNode(NODE_ASSIGN, 2, A, B);
}

expression(E) ::= expression(A) COMMA expression(B).
{
	E = PySN_CreateNode(NODE_COMMA, 2, A, B);
}

expression(E) ::= LEFT_PAR expression(X) RIGHT_PAR.
{
	E = PySN_CreateNode(NODE_PAR, 1, X);
}

expression(E) ::= VAR(V).
{
	E = PySN_CreateNode(NODE_VAR, 0);
	strcpy(E->str, V->str);
}

expression(E) ::= NUM(N).
{
	E = PySN_CreateNode(NODE_NUM, 0);
	strcpy(E->str, N->str);
}

%code {
struct Node *PySN_CreateAST(const char *csn)
{
	struct ParseContext ctx = {0};
	struct Scanner sc = {0};
	struct Token *token = NULL;
	void *parser;

	parser = PySN_ParseAlloc(malloc, &ctx);
	PySN_InitScanner(&sc, csn, strlen(csn));

	while (PySN_NextToken(&sc, &token)) {
		PySN_Parse(parser, token->type, token);
	}

	PySN_Parse(parser, 0, token);
	PySN_ParseFree(parser, free);

	return ctx.result;
}
}
