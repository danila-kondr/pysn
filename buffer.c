#include "buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <assert.h>

void PySN_InitBuffer(struct Buffer *pBuf)
{
	assert(pBuf);

	pBuf->size = 0;
	pBuf->cap = 0;
	pBuf->str = NULL;
}

void PySN_ClearBuffer(struct Buffer *pBuf)
{
	assert(pBuf);

	if (pBuf->str)
		free(pBuf->str);

	PySN_InitBuffer(pBuf);
}

void PySN_BufferPut(struct Buffer *pBuf, void *mem, size_t size)
{
	size_t i;
	uint8_t *bytes;

	if (!mem)
		return;

	if (size == 0)
		return;

	bytes = mem;

	assert(pBuf);

	if (!pBuf->str) {
		pBuf->cap = 16;
		pBuf->str = malloc(pBuf->cap);
		if (!pBuf->str) {
			perror("pysn: error while resizing buffer");
			exit(1);
		}
	}

	if (pBuf->size + size >= pBuf->cap) {
		while (pBuf->size + size >= pBuf->cap)
			pBuf->cap <<= 1;

		pBuf->str = realloc(pBuf->str, pBuf->cap);
		if (!pBuf->str) {
			perror("pysn: error while resizing buffer");
			exit(1);
		}
	}
	
	for (i = 0; i < size; i++) {
		pBuf->str[pBuf->size + i] = bytes[i];
	}
	pBuf->size += size;
}

void PySN_BufferPutString(struct Buffer *pBuf, const char *str)
{
	PySN_BufferPut(pBuf, str, strlen(str));
	pBuf->str[pBuf->size] = '\0';
}
