#include "pysn.h"

#include "token.h"
#include "node.h"
#include "scanner.h"
#include "parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "buffer.h"

static void latexNode(struct Node *x, struct Buffer *pBuf)
{
	assert(pBuf);
#define WRAP2(x, pBuf, a, b, c) do {                                   \
	PySN_BufferPutString(pBuf, a);                                  \
	latexNode(x->ch[0], pBuf);                                     \
	PySN_BufferPutString(pBuf, b);                                  \
	latexNode(x->ch[1], pBuf);                                     \
	PySN_BufferPutString(pBuf, c);                                  \
} while (0)

#define WRAP1(x, pBuf, a, b) do {                                      \
	PySN_BufferPutString(pBuf, a);                                  \
	latexNode(x->ch[0], pBuf);                                     \
	PySN_BufferPutString(pBuf, b);                                  \
} while (0)

#define BETWEEN(x, pBuf, a) do {                                       \
	latexNode(x->ch[0], pBuf);                                     \
	PySN_BufferPutString(pBuf, a);                                  \
	latexNode(x->ch[1], pBuf);                                     \
} while (0)

	switch (x->type) {
	case NODE_ADD: BETWEEN(x, pBuf, "+"); break;
	case NODE_SUB: BETWEEN(x, pBuf, "-"); break;
	case NODE_LT: BETWEEN(x, pBuf, "<"); break;
	case NODE_GT: BETWEEN(x, pBuf, ">"); break;
	case NODE_CDOT:
	case NODE_MUL: BETWEEN(x, pBuf, "\\cdot "); break;
	case NODE_DIV:
		if (x->ch[0]->type == NODE_PAR)
			x->ch[0]->type = NODE_GROUP;
		if (x->ch[1]->type == NODE_PAR)
			x->ch[1]->type = NODE_GROUP;
		WRAP2(x, pBuf, "\\frac{", "}{", "}");
		break;
	case NODE_POWER: WRAP2(x, pBuf, "{", "}^{", "}"); break;
	case NODE_PAR: WRAP1(x, pBuf, "({", "})"); break;
	case NODE_GROUP: WRAP1(x, pBuf, "{", "}"); break;
	case NODE_EQ: BETWEEN(x, pBuf, "="); break;
	case NODE_NE: BETWEEN(x, pBuf, "\\neq "); break;
	case NODE_LE: BETWEEN(x, pBuf, "\\le "); break;
	case NODE_GE: BETWEEN(x, pBuf, "\\ge "); break;
	case NODE_ASSIGN: BETWEEN(x, pBuf, ":="); break;
	case NODE_IN: BETWEEN(x, pBuf, "\\in "); break;
	case NODE_VAR:
	case NODE_NUM: PySN_BufferPutString(pBuf, x->str); break;
	}
#undef WRAP2
#undef WRAP1
#undef BETWEEN
}

static int PySN_GenerateLaTeX(struct Node *tree, char **pResult)
{
	struct Buffer buf = { 0 };

	assert(pResult);
	latexNode(tree, &buf);

	*pResult = buf.str;
	return 1;
}

static int PySN_GenerateMathML(struct Node *tree, char **pResult)
{
	return 0;
}

int PySN(const char *pysn, char **pResult, int outputFormat)
{
	struct Node *tree;
	int ret = 1;

	assert(pResult);

	tree = PySN_CreateAST(pysn);
	if (!tree)
		return 0;

	PySN_DumpTree(tree);
	printf("\n");

	switch (outputFormat) {
	case PYSN_LATEX:
		ret = PySN_GenerateLaTeX(tree, pResult);
		break;
	case PYSN_MATHML:
		ret = PySN_GenerateMathML(tree, pResult);
		break;
	default:
		fprintf(stderr, "pysn: PySN: incorrect output format (%d)\n",
				outputFormat);
	}
	
	PySN_RemoveTree(&tree);
	return ret;
}
