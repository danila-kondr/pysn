#include "node.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

struct Node *PySN_CreateNode(int type, int n_children, ...)
{
	struct Node *result = NULL;
	va_list vl;
	int i;

	if (n_children > MAX_NODE_CHILDREN)
		n_children = MAX_NODE_CHILDREN;

	if (n_children <= 0)
		n_children = 0;

	if (!(result = malloc(sizeof(struct Node)))) {
		perror("pysn: PySN_CreateNode");
		exit(1);
	}

	result->type = type;
	result->n_children = n_children;

	va_start(vl, n_children);
	for (i = 0; i < n_children; i++)
		result->ch[i] = va_arg(vl, struct Node *);
	va_end(vl);

	for (i = 0; i < MAX_TOKEN_LENGTH; i++)
		result->str[i] = '\0';

	return result;
}

void PySN_RemoveTree(struct Node **pRoot)
{
	struct Node *root;
	int i;

	assert(pRoot);

	if (!*pRoot)
		return;
	root = *pRoot;

	for (i = 0; i < root->n_children; i++)
		PySN_RemoveTree(&root->ch[i]);

	free(root);
	*pRoot = NULL;
}

void PySN_DumpTree(struct Node *tree)
{
	int i;
	
	printf("{");
	printf("\"type\": %d, \n", tree->type);
	printf("\"str\": \"");
	for (i = 0; tree->str[i]; i++) {
		switch (tree->str[i]) {
		case '"':
			printf("\\");
			break;
		case '\\':
			printf("\\\\");
			break;
		case '\a':
			printf("\\a");
			break;
		case '\b':
			printf("\\b");
			break;
		case '\f':
			printf("\\f");
			break;
		case '\n':
			printf("\\n");
			break;
		case '\r':
			printf("\\r");
			break;
		case '\t':
			printf("\\t");
			break;
		case '\v':
			printf("\\v");
			break;
		default:
			if (tree->str[i] >= 0x20 && tree->str[i] <= 0x7f)
				printf("%c", tree->str[i]);
			else
				printf("\\x%hhX", tree->str[i]);
		}
	}
	printf("\"");
	if (tree->n_children) {
		printf(", \"children\": [");
		for (i = 0; i < tree->n_children; i++) {
			PySN_DumpTree(tree->ch[i]);
			if (i < tree->n_children - 1)
				printf(", ");
		}
		printf("]");
	}
	printf("}");
}
