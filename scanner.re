#include "scanner.h"

#include <assert.h>
#include "parser.h"

void PySN_InitScanner(struct Scanner *scanner, const char *input, size_t len)
{
	assert(scanner);

	scanner->cursor = input;
	scanner->limit = input + len;
	scanner->marker = input;
}

int PySN_NextToken(struct Scanner *scanner, struct Token **pToken)
{
	assert(scanner);
	assert(pToken);

#define STRING(scanner) ((scanner)->marker )
#define STRLEN(scanner) ( ((scanner)->cursor) - ((scanner)->marker) )
#define TOKEN(_id, _str, _n) do {                                      \
	if (_n)                                                        \
		*pToken = PySN_CreateTokenN(_id, _str, _n);            \
	scanner->marker = scanner->cursor;                             \
	return _id;                                                    \
} while (0)

begin:;
	/*!re2c
re2c:define:YYCTYPE = char;
re2c:define:YYCURSOR = scanner->cursor;
re2c:define:YYMARKER = scanner->marker;
re2c:define:YYLIMIT = scanner->limit;
re2c:yyfill:enable = 0;
re2c:eof = 0;

"cdot" { TOKEN(TOKEN_CDOT, "cdot", 4); }
"in"   { TOKEN(TOKEN_IN, "in", 2); }

[A-Za-z_][A-Za-z0-9_]* { TOKEN(TOKEN_VAR, STRING(scanner), STRLEN(scanner)); }
[0-9]+ { TOKEN(TOKEN_NUM, STRING(scanner), STRLEN(scanner)); }

"+"   { TOKEN(TOKEN_PLUS, "+", 1); }
"-"   { TOKEN(TOKEN_MINUS, "-", 1); }
"*"   { TOKEN(TOKEN_MULTIPLY, "*", 1); }
"/"   { TOKEN(TOKEN_DIVIDE, "/", 1); }

"=="  { TOKEN(TOKEN_EQ, "==", 2); }
"!="  { TOKEN(TOKEN_NE, "!=", 2); }
"<="  { TOKEN(TOKEN_LE, "<=", 2); }
">="  { TOKEN(TOKEN_GE, ">=", 2); }
"<"   { TOKEN(TOKEN_LT, "<", 1); }
">"   { TOKEN(TOKEN_GT, ">", 1); }

"="   { TOKEN(TOKEN_ASSIGN, "=", 1); }

"("   { TOKEN(TOKEN_LEFT_PAR, "(", 1); }
")"   { TOKEN(TOKEN_RIGHT_PAR, ")", 1); }
","   { TOKEN(TOKEN_COMMA, ",", 1); }
";"   { TOKEN(TOKEN_SEMICOLON, ";", 1); }

[ \t\r\n]+ { scanner->marker = scanner->cursor; goto begin; }

$     { TOKEN(TOKEN_END, "", 0); }

	*/

end:;
#undef TOKEN
#undef STRING
#undef STRLEN
}

#if 0
vim: syntax=c
#endif
