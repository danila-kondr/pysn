#ifndef _PYSN_H_
#define _PYSN_H_

enum OutputFormat {
	PYSN_LATEX,
	PYSN_MATHML,
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Open API for Python Standard Notation parser
 *
 * pysn:             input string which contains 
 *                  formula in Python Standard Notation
 *
 * pResult:         pointer to result zero-terminated string;
 *                  if pResult == NULL, string will be allocated
 *
 * outputFormat:    type of output format
 *                  PYSN_LATEX:  LaTeX
 *                  PYSN_MATHML: MathML
 *
 * Returns 1 if success, 0 if fail.
 */
int PySN(const char *pysn, char **pResult, int outputFormat);

#ifdef __cplusplus
}
#endif

#endif
