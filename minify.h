#ifndef _MINIFY_H_
#define _MINIFY_H_

#include "node.h"

void PySN_MinifyAST(struct Node *root);

#endif
