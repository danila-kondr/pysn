#ifndef _NODE_H_
#define _NODE_H_

#include "const.h"

enum NodeType {
	NODE_NOTHING,
	NODE_ADD,
	NODE_SUB,
	NODE_MUL,
	NODE_DIV,
	NODE_CDOT,
	NODE_EQ,
	NODE_NE,
	NODE_LE,
	NODE_GE,
	NODE_LT,
	NODE_GT,
	NODE_ASSIGN,
	NODE_COMMA,
	NODE_PAR,
	NODE_VAR,
	NODE_NUM,
	NODE_POWER,
	NODE_GROUP,
	NODE_IN,
};

#define MAX_NODE_CHILDREN 4

struct Node {
	int type;
	char str[MAX_TOKEN_LENGTH];

	int n_children;
	struct Node *ch[MAX_NODE_CHILDREN];
};

struct Node *PySN_CreateNode(int type, int n_children, ...);
void PySN_RemoveTree(struct Node **pRoot);

void PySN_DumpTree(struct Node *root);

#endif
