#include <stdio.h>

#include "pysn.h"

const char input1[] = "b*b-4*a*c";
const char input2[] = "1+2==3";

int main() {
	char *test = NULL;

#define TEST(input) do {                                               \
		PySN(input, &test, PYSN_LATEX);                        \
                                                                       \
		printf("Input:  %s\n", input);                         \
		printf("Result: %s\n\n", test);                        \
                                                                       \
		free(test);                                            \
	} while (0)

	TEST(input1);
	TEST(input2);
	TEST("a*b*c*d*e*f*g*h");
	TEST("4 * 8 == 32");
	TEST("4 cdot 8 == 32");
	TEST("a*b/c*d");
	TEST("(a*b)/(c*d)");

	return 0;
}
