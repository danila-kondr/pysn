/* This file was automatically generated.  Do not edit! */
#undef INTERFACE
#include <stdio.h>
int PySN_ParseFallback(int iToken);
#define PySN_ParseTOKENTYPE  struct Token * 
#define PySN_ParseARG_PDECL
void PySN_Parse(void *yyp,int yymajor,PySN_ParseTOKENTYPE yyminor PySN_ParseARG_PDECL);
#if defined(YYCOVERAGE)
int PySN_ParseCoverage(FILE *out);
#endif
#if defined(YYTRACKMAXSTACKDEPTH)
int PySN_ParseStackPeak(void *p);
#endif
#if !defined(PySN_Parse_ENGINEALWAYSONSTACK)
void PySN_ParseFree(void *p,void(*freeProc)(void *));
#endif
void PySN_ParseFinalize(void *p);
typedef struct ParseContext ParseContext;
#define PySN_ParseCTX_PDECL , struct ParseContext *ctx 
#if !defined(PySN_Parse_ENGINEALWAYSONSTACK)
void *PySN_ParseAlloc(void *(*mallocProc)(YYMALLOCARGTYPE)PySN_ParseCTX_PDECL);
#endif
void PySN_ParseInit(void *yypRawParser PySN_ParseCTX_PDECL);
#if !defined(NDEBUG)
void PySN_ParseTrace(FILE *TraceFILE,char *zTracePrompt);
#endif
#define PySN_ParseCTX_STORE yypParser->ctx =ctx ;
#define PySN_ParseCTX_FETCH  struct ParseContext *ctx =yypParser->ctx ;
#define PySN_ParseCTX_PARAM ,ctx 
#define PySN_ParseCTX_SDECL  struct ParseContext *ctx ;
#define PySN_ParseARG_STORE
#define PySN_ParseARG_FETCH
#define PySN_ParseARG_PARAM
#define PySN_ParseARG_SDECL
#define TOKEN_NUM                            19
#define TOKEN_VAR                            18
#define TOKEN_SEMICOLON                      17
#define TOKEN_RIGHT_PAR                      16
#define TOKEN_LEFT_PAR                       15
#define TOKEN_COMMA                          14
#define TOKEN_ASSIGN                         13
#define TOKEN_CDOT                           12
#define TOKEN_DIVIDE                         11
#define TOKEN_MULTIPLY                       10
#define TOKEN_MINUS                           9
#define TOKEN_PLUS                            8
#define TOKEN_IN                              7
#define TOKEN_GT                              6
#define TOKEN_LT                              5
#define TOKEN_GE                              4
#define TOKEN_LE                              3
#define TOKEN_NE                              2
#define TOKEN_EQ                              1
struct Node *PySN_CreateAST(const char *pysn);
struct Node *PySN_CreateAST(const char *pysn);
struct ParseContext {
	struct Node *result;
};
#define INTERFACE 0
