#ifndef _TOKEN_H_
#define _TOKEN_H_

#include <stddef.h>
#include "const.h"

#define TOKEN_END 0

struct Token {
	int type;
	char str[MAX_TOKEN_LENGTH];
};

struct Token *PySN_CreateToken(int type, const char *str);
struct Token *PySN_CreateTokenN(int type, const char *str, size_t len);

void PySN_InitToken(struct Token *pToken, int type, const char *str);
void PySN_InitTokenN(struct Token *pToken, int type,
	const char *str, size_t len);

#endif
