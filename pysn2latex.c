#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pysn.h"

int main(int argc, char **argv)
{
	char *pysn, *result = NULL;

	if (argc < 2) {
		printf("%s formula\n", argv[0]);
		exit(1);
	}

	pysn = argv[1];

	if (!PySN(pysn, &result, PYSN_LATEX)) {
		printf("%s: error during parsing formula\n");
		exit(1);
	}

	printf("%s\n", result);
	free(result);

	return 0;
}
